[![stability-beta](https://img.shields.io/badge/stability-beta-33bbff.svg)](https://github.com/mkenney/software-guides/blob/master/STABILITY-BADGES.md#beta)
[![Python 3.6](https://img.shields.io/badge/python-3.7%20%7C%203.8-brightgreen)](https://www.python.org/downloads/release/python-370/)
[![License](https://img.shields.io/pypi/l/locscale.svg?color=orange)](https://gitlab.tudelft.nl/aj-lab/locscale/raw/master/LICENSE)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6652013.svg)](https://doi.org/10.5281/zenodo.6652013)

# q4STEM: Robust local thickness estimation of sub-micrometer specimen by 4D-STEM

`q4STEM` is an analysis tool for thickness estimation of FIB-milled lamella for TEM. q4STEM utilises spatially resolved diffraction patterns to obtain the angular distribution of electron scattering, or the ratio of integrated virtual dark and bright field STEM signals for their quantitative evaluation using Monte Carlo simulations.

See [here](edu.nl/6nyfn) for a detailed explanation of the method.

<br>
  
`q4STEM` is distributed as a portable stand-alone installation that includes all the needed libraries from: https://gitlab.tudelft.nl/aj-lab/q4stem/releases.   

## Installation 

We recommend to use [Conda](https://docs.conda.io/en/latest/) for a local working environment. See [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/download.html#anaconda-or-miniconda) for more information on what Conda flavour may be the right choice for you, and [here](https://www.anaconda.com/products/distribution) for Conda installation instructions.

#### Requirements

q4STEM should run on any CPU system with Linux, OS X or Windows subsytem for Linux (WSL). 

#### Installation & usage:

##### 1. Create and activate a new conda environment

```bash
conda env create environemnt.yml
conda activate q4stem
```

##### 2. Run q4stem notebook

```bash
jupyter notebook q4stem_run.ipynb
```
